# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Website: [Post it!][my web]
* Key functions (Users should login first)
   1. Post new ariticles
   2. Comment under any ariticle
   3. See only your articles in user page

* Other functions (add/delete)
   1. No

[my web]: https://105062315.gitlab.io/Midterm_Project/index.html

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|N|

## Website Detail Description
   1. Login or sign-up on upper right corner (google and facebook login supported)    
   ![sign in](reportImg/signup.PNG)
   2. If you choose "sign-up", the login pop up will change into sign up view 
   ![sign up](reportImg/signup.PNG)
   3. A small animation will show when posts are loading 
   ![loading animation](reportImg/loadingAnimation.PNG)
   4. View all posts and comments after sign in 
   ![home page](reportImg/readpost.PNG)
   5. Click "New post" to upload new ariticle 
   ![new post](reportImg/newpost.PNG)
   6. Feel free to comment under any posts!    
   ![new comment](reportImg/leavecomment.PNG)
   7. Click user name or "My post" link to jump to user page    
   ![user page](reportImg/userpage.PNG)
   8. You can see all your posts here 
   9. Click "All posts" or "Post it" mark to return to main page    
   ![back to home](reportImg/backtohomepage.PNG)

## Security Report (Optional)    
   ![rule](reportImg/ruleScreenShot.PNG)
   1. Only users themselves can update their user data
   2. Only users themselves can update user post list
   3. Posts data are accessible only after users log in
   4. Posts and comments are uneditable after release
