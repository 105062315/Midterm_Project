
var postLoadingTool = {
	postCount: 0,
	postLoadCount: 0,
	postData: [],
	/* reset the tool variable */
	setPostHTML: function (){
		var articleList = document.querySelector("#article-list");
		articleList.innerHTML = postData.join("");
	},
	addPostCount: function () {
		postCount++;
	},
	loadPost: function (postHTML){
		postLoadCount++;
		postData.unshift(postHTML);
		/* if all loading has done */
		if(postLoadCount>=postCount){
			this.setPostHTML();
		}
	},
	resetTool: function (){
		postCount = 0;
		postLoadCount = 0;
		postData = [];
	}
}

// user press logout button
function userLogout(){
	firebase.auth().signOut()
		.then(function(){
			alert("Sign out successfully!");
			window.location = "index.html";
		})
		.catch(e => {
			console.log("Fail to logout. " + e.message);
			alert("Something wrong when logout. Please try again.");
		});
}

function changeNavBar(user){
	var navInfo = document.querySelector("#userInfoNav");
	
	if(user){
		navInfo.innerHTML = 
			'<span id="userNavName">' + user.displayName + '</span> \
			<button class="btn-dark btn" type="button" id="logoutBtn">Logout</button>';
		
		// listen logout action
		document.querySelector("#logoutBtn").addEventListener("click", userLogout);
	}else {
		console.log("Fail to load navBar. Couldn't access user info.");
	}
}

// show sidebar info
function showUserInfo(user){
	var userInfo = document.querySelector(".userInfo");
	
	if(user){
		userInfo.innerHTML = 
		'<img class="img-thumbnail" id="userPhoto" src="' + user.photoURL + '"/> \
		 <a class="title" id="userName" href="userpage.html"> ' + user.displayName + ' </a> \
		 <p>' + user.email + '</p> \
		 <p id="userIntro"> Intro. </p>';
	}else{
		console.log("Fail to load sideBar. Couldn't access user info.");
	}
}

/* generate comment HTML string */
function generateComment(commentList) {
	if(commentList){
		var commentHTMLList = [];
		for(commentKey in commentList){
			commentHTMLList.push(
				'<div class="comment-block"> \
					<p> ' + commentList[commentKey].content + '</p> \
					<p class="comment-author"> —— @' + commentList[commentKey].author + ' <span class="date"> ' + commentList[commentKey].date + '</span></p> \
				</div>');
		}
		return commentHTMLList.join("<hr/>");
	}
	else{
		return "";
	}
}

/* generate post as HTML string */
function generatePost(postKey) {
	// get post data
	firebase.database().ref("/posts/" + postKey).once("value")
		.then( function(snapshot){
			var authorName = snapshot.val().author;
			var authorEmail = snapshot.val().email;
			var date = snapshot.val().date;
			var title = snapshot.val().title;
			var content = snapshot.val().content;
			var comment = generateComment(snapshot.val().comments);
			
			var post = 
				'<div class="article"> \
					<h3 class="article-title">' + title + '</h3> \
					<p class="article-author">@' + authorName + ' <span class="email">(' + authorEmail + ')</span><span class="date">' + date + '</span></p> \
					<hr/> \
					<p class="article-content"><pre>' + content + '</pre></p> \
					<hr/> \
					<h5 class="comment-title">Comment</h5> \
					<div class="comment-list">' + comment + '</div> \
				</div>';
			
			postLoadingTool.loadPost(post);
		})
		.catch(e => {
			console.log(e.message);
		});
}

function loadPost(user, type) {
	var postDataRef;
	var postKeyList = [];
	
	if(user){
		if(type==="all") {
			postDataRef = firebase.database().ref("posts");
		}else if(type==="user") {
			postDataRef = firebase.database().ref("/user-posts/" + user.uid);
		}
		
		postDataRef.once("value")
			.then( snapshot => {
				for(postKey in snapshot.val()){
					// record how many posts there are
					postLoadingTool.addPostCount();
					postKeyList.push(postKey);
				}
				if(postKeyList.length==0){
					// if no post
					var articleList = document.querySelector("#article-list");
					articleList.innerHTML = 
						'<div class="article"> \
							<p class="article-content"><pre> No posts recently. </pre></p> \
						</div>';
				}else{
					// get post from database
					postKeyList.forEach((postKey) => {
						generatePost(postKey);
					});
				}
			})
			.catch(e => {
				console.log(e.message);
			});
	}else {
		alert("Please login first!");
		window.location = "index.html";
	}
}

function loadPage() {
	var navInfo = document.querySelector("#userInfoNav");
	firebase.auth().onAuthStateChanged(function (user) {
		if(user){
			// if user logged in
			changeNavBar(user);
			postLoadingTool.resetTool();
		}else{
			alert("Please login first!");
			window.location = "index.html";
		}
		showUserInfo(user);
		loadPost(user, "user");
	});
}

window.onload = function () {
    loadPage();
};
